<?php
/**
 * Created by PhpStorm.
 * User: malte
 * Date: 22.11.2019
 * Time: 21:03
 */

namespace MabaGroup\Cairo\Models;


/**
 * Class Person
 * @package MabaGroup\Cairo\Models
 */
class Person
{
    /**
     * @var
     */
    public $salutation;

    /**
     * @var
     */
    public $firstName;

    /**
     * @var
     */
    public $lastName;
}