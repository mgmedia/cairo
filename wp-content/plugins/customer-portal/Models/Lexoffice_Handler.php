<?php

namespace MabaGroup\Cairo\Models;

/**
 * Class Lexoffice_Handler
 * @package MabaGroup\Cairo\Models
 */
class Lexoffice_Handler
{
    /**
     * @var
     */
    private $base_url;
    /**
     * @var
     */
    private $api_key;

    /**
     * Lexoffice_Handler constructor.
     *
     * @param $base_url
     * @param $api_key
     */
    public function __construct($base_url, $api_key)
    {
        $this->base_url = $base_url;
        $this->api_key  = $api_key;
    }

    /**
     *
     */
    public function SyncAll(): void
    {
    }

    /**
     * @return array
     */
    public function GetContacts(): array
    {
        $options = array(
            "http" => array(
                "header" => array(
                    "Authorization: Bearer {$this->api_key}",
                    "Accept: application/json",
                ),
            ),
        );

        $context = stream_context_create($options);
        if ($result = file_get_contents($this->base_url."/v1/contacts?size=100", false, $context)) {
            $result = json_decode($result)->content;
        } else {
            $result = array();
        }

        if (count($result) === 0) {
            return array();
        }

        return $result;
    }

    /**
     *
     */
    public function SyncContacts(): void
    {
        $contacts = $this->GetContacts();

        foreach ($contacts as $contact) {
            // Check if Contact Exists By Id
            $existing_contacts = get_posts(
                array(
                    "post_type"  => "contact",
                    "meta_key"   => "id",
                    "meta_value" => $contact->id,
                )
            );

            // Check if version is different
            if (count($existing_contacts) > 0) {
                if ($this->contactNeedsUpdate($existing_contacts[0], $contact)) {
                    $this->deleteContact($existing_contacts);
                } else {
                    continue;
                }
            }

            new Contact($contact);
        }
    }

    /**
     * @param $contact_post
     * @param $api_contact
     *
     * @return bool
     */
    private function contactNeedsUpdate($contact_post, $api_contact): bool
    {
        $contact_post_version = get_field("version", $contact_post);

        if ($api_contact->version > $contact_post_version) {
            return true;
        }

        return false;
    }

    /**
     * @param $contact_post
     */
    private function deleteContact($contact_post)
    {
        wp_delete_post($contact_post);
    }
}