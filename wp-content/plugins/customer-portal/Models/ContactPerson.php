<?php
/**
 * Created by PhpStorm.
 * User: malte
 * Date: 22.11.2019
 * Time: 21:14
 */

namespace MabaGroup\Cairo\Models;


/**
 * Class ContactPerson
 * @package MabaGroup\Cairo\Models
 */
class ContactPerson extends Person
{
    /**
     * @var
     */
    public $emailAddress;
}