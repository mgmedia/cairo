<?php


namespace MabaGroup\Cairo\Models;


/**
 * Class Role
 * @package MabaGroup\Cairo\Models
 */
class Role
{
    /**
     * @var $customer Customer
     */
    public $customer;

    /**
     * @var $vendor Vendor
     */
    public $vendor;
}