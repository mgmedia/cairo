<?php
/**
 * Created by PhpStorm.
 * User: malte
 * Date: 22.11.2019
 * Time: 20:35
 */

namespace MabaGroup\Cairo\Models;

use stdClass;
use WP_Post;

/**
 * Class Contact
 * @package MabaGroup\Cairo\Models
 */
class Contact
{
    /**
     * @var
     */
    private $id;

    /**
     * @var
     */
    private $organizationId;

    /**
     * @var
     */
    private $version;

    /**
     * @var
     */
    private $roles;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var
     */
    private $person;

    /**
     * @var $addresses Address[]
     */
    private $addresses;

    /**
     * @var
     */
    private $emailAddresses;

    /**
     * @var
     */
    private $phoneNumbers;

    /**
     * @var
     */
    private $archived;


    /**
     * Contact constructor.
     * @param string $contact
     */
    public function __construct($contact = "")
    {
        if ($contact === "") {
            return;
        }

        $this->initialize_fields_by_api($contact);
        $this->createWPPost();
        echo "Contact has been created.\n";
    }

    /**
     *
     */
    public function render_contact()
    {
        ?>
        <div class="User">
            <h3><?php $this->the_primary_contact_name(); ?></h3>
            <?php $this->the_fields_list(); ?>
        </div>
        <?php
    }

    /**
     *
     */
    public function the_customer_number(): void
    {
        echo $this->get_the_customer_number();
    }

    /**
     *
     */
    public function the_vendor_number(): void
    {
        echo $this->get_the_vendor_number();
    }

    /**
     * @return int
     */
    public function get_the_customer_number(): int
    {
        if (isset($this->roles->customer->number)) {
            return $this->roles->customer->number;
        }

        return 0;
    }

    /**
     * @return int
     */
    public function get_the_vendor_number(): int
    {
        if (isset($this->roles->vendor->number)) {
            return $this->roles->vendor->number;
        }

        return 0;
    }

    /**
     * @param stdClass $contact
     */
    private function initialize_fields_by_api(stdClass $contact): void
    {
        if (isset($contact->id)) {
            $this->id = $contact->id;
        }

        if (isset($contact->organizationId)) {
            $this->organizationId = $contact->organizationId;
        }

        if (isset($contact->version)) {
            $this->version = $contact->version;
        }

        if (isset($contact->roles)) {
            $this->roles = $contact->roles;
        }

        if (isset($contact->company)) {
            $this->company = new Company();

            if (isset($contact->company->name)) {
                $this->company->name = $contact->company->name;
            }

            if (isset($contact->company->vatRegistrationId)) {
                $this->company->vatRegistrationId = $contact->company->vatRegistrationId;
            }

            if (isset($contact->company->allowTaxFreeInvoices)) {
                $this->company->allowTaxFreeInvoices = $contact->company->allowTaxFreeInvoices;
            }

            if (isset($contact->company->contactPersons)) {
                $this->company->contactPersons = $contact->company->contactPersons;
            }
        }

        if (isset($contact->person)) {
            $this->person = new Person();
            $this->person->salutation = $contact->person->salutation;
            $this->person->firstName = $contact->person->firstName;
            $this->person->lastName = $contact->person->lastName;
        }

        if (isset($contact->addresses)) {
            $this->addresses = $contact->addresses;
        }

        if (isset($contact->emailAddresses)) {
            $this->emailAddresses = new EMailAddress();
            if (isset($this->emailAddresses->business)) {
                $this->emailAddresses->business = $contact->emailAddresses->business;
            }
        }

        if (isset($contact->phoneNumbers)) {
            $this->phoneNumbers = $contact->phoneNumbers;
        }

        if (isset($contact->archived)) {
            $this->archived = $contact->archived;
        }
    }

    /**
     * @return Person
     */
    public function get_the_person()
    {
        if (isset($this->person)) {
            return $this->person;
        }

        return null;
    }

    /**
     * @return Company
     */
    public function get_the_company()
    {
        if (isset($this->company)) {
            return $this->company;
        }

        return null;
    }

    /**
     *
     */
    public function the_primary_contact_name()
    {
        echo $this->get_the_primary_contact_name();
    }

    /**
     * @return string|null
     */
    public function get_the_primary_contact_name()
    {
        if ($this->get_the_person()) {
            return sprintf(
                "%s %s %s",
                $this->get_the_person()->salutation,
                $this->get_the_person()->firstName,
                $this->get_the_person()->lastName
            );
        }

        if ($this->get_the_company()) {
            return sprintf("%s", $this->get_the_company()->name);
        }

        return null;
    }

    /**
     * @return int
     */
    public function get_the_contact_number(): int
    {
        if (isset($this->roles->customer)) {
            return $this->roles->customer->number;
        } elseif (isset($this->roles->vendor)) {
            return $this->roles->vendor->number;
        }

        return 0;
    }

    /**
     *
     */
    public function the_fields_list()
    {
        $keys_and_elements = array();

        if ($this->id) {
            $keys_and_elements["id"] = $this->id;
        }

        $keys_and_elements["No."] = $this->get_the_contact_number();

        if (isset($this->organizationId)) {
            $keys_and_elements["organizationId"] = $this->organizationId;
        }

        if (isset($this->version)) {
            $keys_and_elements["version"] = $this->version;
        }

        if (isset($this->emailAddresses)) {
            $keys_and_elements["emailAddress"] = $this->get_contact_email_addresses_as_list();
        }

        if (count($keys_and_elements) > 0) : ?>
            <dl>
                <?php foreach ($keys_and_elements as $key => $value) : ?>
                    <dt><?php echo $key ?></dt>
                    <dd><?php echo $value; ?></dd>
                <?php endforeach; ?>
            </dl>
        <?php endif;
    }

    /**
     * @return string
     */
    public function get_contact_email_addresses_as_list(): string
    {
        $str = "";
        $open_li = "<li>";
        $close_li = "</li>";

        if (isset($this->emailAddresses->business)) {
            $str .= "<ul>";
            foreach ($this->emailAddresses->business as $business_mail_address) {
                $str .= $open_li . $business_mail_address . $close_li;
            }
            $str .= "</ul>";
        }

        return $str;
    }

    /**
     *
     */
    private function createWPPost(): void
    {
        $args = array(
            "guid" => $this->id,
            "post_type" => "contact",
            "post_title" => $this->get_the_primary_contact_name(),
            "post_status" => "publish",
        );

        $postId = wp_insert_post($args);
        update_field("id", $this->id, $postId);
        update_field("organizationid", $this->organizationId, $postId);
        update_field("version", $this->version, $postId);
        update_field("archived", $this->archived, $postId);

        update_field("customer_no", $this->get_the_customer_number(), $postId);
        update_field("vendor_no", $this->get_the_vendor_number(), $postId);
        update_field("vat_registration_no", $this->getVATRegistrationId(), $postId);
        update_field("allow_tax_free_invoices", $this->getAllowTaxFreeInvoices(), $postId);

        foreach ($this->getCompanyContactPersons() as $contactPerson) {
            $args = array();

            if (isset($contactPerson->salutation)) {
                $args["contact_person_salutation"] = $contactPerson->salutation;
            }

            if (isset($contactPerson->firstName)) {
                $args["contact_person_first_name"] = $contactPerson->firstName;
            }

            if (isset($contactPerson->lastName)) {
                $args["contact_person_last_name"] = $contactPerson->lastName;
            }

            if (isset($contactPerson->emailAddress)) {
                $args["contact_person_email_address"] = $contactPerson->emailAddress;
            }

            add_row("contact_persons", $args, $postId);
        }


        update_field("company", json_encode($this->company), $postId);
        update_field("person", json_encode($this->person), $postId);
        update_field("addresses", json_encode($this->addresses), $postId);
        update_field("emailaddresses", json_encode($this->emailAddresses), $postId);
        update_field("phonenumbers", json_encode($this->phoneNumbers), $postId);
    }

    /**
     * @return string
     */
    public function getVATRegistrationId()
    {
        if (isset($this->company)) {
            return $this->company->vatRegistrationId;
        }

        return "";
    }

    /**
     * @return string
     */
    public function getAllowTaxFreeInvoices(): string
    {
        if (isset($this->company->allowTaxFreeInvoices)) {
            return $this->company->allowTaxFreeInvoices;
        }

        return "";
    }

    /**
     * @return array
     */
    public function getCompanyContactPersons(): array
    {
        if (isset($this->company->contactPersons)) {
            return $this->company->contactPersons;
        }

        return array();
    }

    /**
     * @param WP_Post $post
     * @return Contact
     */
    public static function getContactByWPPost(WP_Post $post): Contact
    {
        $contact = new Contact();
        $contact->id = get_field("id", $post->ID);
        $contact->organizationId = get_field("organization_id", $post->ID);
        $contact->version = get_field("version", $post->ID);

        $customer_no = get_field("customer_no", $post->ID);
        $vendor_no = get_field("vendor_no", $post->ID);

        $role = new Role();
        if ($customer_no !== 0) {
            $role->customer = new Customer();
            $role->customer->number = $customer_no;
        }

        if ($vendor_no !== 0) {
            $role->vendor = new Vendor();
            $role->vendor->number = $vendor_no;
        }

        $contact->company = new Company();
        $contact->company->allowTaxFreeInvoices = get_field("allowtaxfreeinvoices", $post->ID);
        $contact->company->name = get_the_title($post->ID);
        $contact->company->vatRegistrationId = get_field("vatregistrationid", $post->ID);

        $contactPersons = [];
        $contactPersonRow = get_field("contact_persons", $post->ID);
        if (is_countable($contactPersonRow)) {
            foreach ($contactPersonRow as $contactPersonItem) {
                $contactPerson = new ContactPerson();
                $contactPerson->emailAddress = $contactPersonItem["contact_person_email_address"];
                $contactPerson->firstName = $contactPersonItem["contact_person_first_name"];
                $contactPerson->lastName = $contactPersonItem["contact_person_last_name"];
                $contactPerson->salutation = $contactPersonItem["contact_person_salutation"];
                $contactPersons[] = $contactPerson;
            }
        }

        $contact->company->contactPersons = $contactPersons;

        return $contact;
    }
}