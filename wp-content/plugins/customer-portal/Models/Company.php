<?php
/**
 * Created by PhpStorm.
 * User: malte
 * Date: 22.11.2019
 * Time: 21:13
 */

namespace MabaGroup\Cairo\Models;


/**
 * Class Company
 * @package MabaGroup\Cairo\Models
 */
class Company
{
    /**
     * @var
     */
    public $name;

    /**
     * @var
     */
    public $vatRegistrationId;

    /**
     * @var
     */
    public $allowTaxFreeInvoices;

    /**
     * @var ContactPerson[]
     */
    public $contactPersons;
}