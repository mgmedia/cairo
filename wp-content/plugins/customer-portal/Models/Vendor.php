<?php


namespace MabaGroup\Cairo\Models;


/**
 * Class Vendor
 * @package MabaGroup\Cairo\Models
 */
class Vendor
{
    /**
     * @var $number int
     */
    public $number;
}