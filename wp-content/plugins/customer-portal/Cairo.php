<?php
/*
 * Plugin Name: CAIRO - Customer Portal
 * Author: MabaGroup Multimedia UG (haftungsbeschränkt)
 * Author URI: https://maba-group.com/
 */

/** @noinspection PhpIncludeInspection */

namespace MabaGroup\Cairo;

use MabaGroup\Cairo\Controllers\ContactController;
use MabaGroup\Cairo\Models\Lexoffice_Handler;

/**
 * Class Cairo
 * @package MabaGroup\Cairo
 */
class Cairo
{
    /**
     *
     */
    public static function boot()
    {
        add_action("init", array("MabaGroup\\Cairo\\Cairo", "deploy_vouchers"));
        add_action("init", array("MabaGroup\\Cairo\\Cairo", "deploy_contacts"));

        spl_autoload_register(array("MabaGroup\\Cairo\\Cairo", "autoload"));

        $configuration = include "config.php";
        $lexoffice_handler = new Lexoffice_Handler($configuration["base_url"], $configuration["api_key"]);

        add_action("activate_" . plugin_basename(__FILE__), array("MabaGroup\Cairo\Cairo", "activate_plugin"));
        add_action("mabagroup_cairo_sync_contacts", array($lexoffice_handler, "SyncContacts"));
        add_filter("the_content", array("MabaGroup\\Cairo\\Cairo", "render_account_page"), 10, 1);
    }

    /**
     *
     */
    public static function activate_plugin()
    {
        if (!wp_next_scheduled("mabagroup_cairo_sync_contacts")) {
            wp_schedule_event(time(), "hourly", "mabagroup_cairo_sync_contacts");
        }
    }

    /**
     * @param $filename string
     */
    private static function autoload($filename)
    {
        $files_array = explode("\\", $filename);
        $i = count($files_array);
        if ($i > 1) {
            require plugin_dir_path(__FILE__) . $files_array[$i - 2] . '/' . $files_array[$i - 1] . ".php";
        }
    }

    /**
     *
     */
    public static function deploy_vouchers()
    {
        $args = array(
            "label" => "Voucher",
            "public" => true,
            "exclude_from_search" => false,
            "supports" => array(
                "title",
            ),
        );

        register_post_type("voucher", $args);

        if (function_exists('acf_add_local_field_group')):
            acf_add_local_field_group(array(
                'key' => 'group_5dd6964fd7177',
                'title' => 'Voucher',
                'fields' => array(
                    array(
                        'key' => 'field_5dd69654044b0',
                        'label' => 'ID',
                        'name' => 'id',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5dd696e6044b1',
                        'label' => 'voucherType',
                        'name' => 'vouchertype',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'invoice' => 'invoice',
                            'creditnote' => 'creditnote',
                        ),
                        'default_value' => array(),
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'return_format' => 'value',
                        'ajax' => 0,
                        'placeholder' => '',
                    ),
                    array(
                        'key' => 'field_5dd69718044b2',
                        'label' => 'voucherStatus',
                        'name' => 'voucherstatus',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'draft' => 'draft',
                            'open' => 'open',
                            'paid' => 'paid',
                            'paidoff' => 'paidoff',
                            'voided' => 'voided',
                            'transferred' => 'transferred',
                            'overdue' => 'overdue',
                        ),
                        'default_value' => array(),
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'return_format' => 'value',
                        'ajax' => 0,
                        'placeholder' => '',
                    ),
                    array(
                        'key' => 'field_5dd69745044b3',
                        'label' => 'voucherNumber',
                        'name' => 'vouchernumber',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5dd6975a044b4',
                        'label' => 'voucherDate',
                        'name' => 'voucherdate',
                        'type' => 'date_time_picker',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'display_format' => "Y-m-dTH:i:s",
                        'return_format' => "Y-m-dTH:i:s",
                        'first_day' => 1,
                    ),
                    array(
                        'key' => 'field_5dd697b5044b5',
                        'label' => 'updatedDate',
                        'name' => 'updateddate',
                        'type' => 'date_time_picker',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'display_format' => "Y-m-dTH:i:s",
                        'return_format' => "Y-m-dTH:i:s",
                        'first_day' => 1,
                    ),
                    array(
                        'key' => 'field_5dd697e5044b6',
                        'label' => 'dueDate',
                        'name' => 'duedate',
                        'type' => 'date_time_picker',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'display_format' => "Y-m-dTH:i:s",
                        'return_format' => "Y-m-dTH:i:s",
                        'first_day' => 1,
                    ),
                    array(
                        'key' => 'field_5dd69802044b7',
                        'label' => 'contactName',
                        'name' => 'contactname',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5dd6980b044b8',
                        'label' => 'totalAmount',
                        'name' => 'totalamount',
                        'type' => 'number',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'min' => '',
                        'max' => '',
                        'step' => '',
                    ),
                    array(
                        'key' => 'field_5dd6981c044b9',
                        'label' => 'currency',
                        'name' => 'currency',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5dd69828044ba',
                        'label' => 'archived',
                        'name' => 'archived',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'voucher',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));
            acf_add_local_field_group(array(
                'key' => 'group_5ddb9998697b3',
                'title' => 'CAIRO User Meta Fields',
                'fields' => array(
                    array(
                        'key' => 'field_5ddb99c7a1802',
                        'label' => 'Customer No.',
                        'name' => 'customer_no',
                        'type' => 'number',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'min' => '',
                        'max' => '',
                        'step' => '',
                    ),
                    array(
                        'key' => 'field_5ddb99d6a1803',
                        'label' => 'Vendor No.',
                        'name' => 'vendor_no',
                        'type' => 'number',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'min' => '',
                        'max' => '',
                        'step' => '',
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'user_form',
                            'operator' => '==',
                            'value' => 'edit',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));
        endif;
    }

    /**
     *
     */
    public static function deploy_contacts()
    {
        $args = array(
            "label" => "Contact",
            "public" => true,
            "exclude_from_search" => false,
            "supports" => array(
                "title",
            ),
        );

        register_post_type("contact", $args);

        if (function_exists('acf_add_local_field_group')):
            acf_add_local_field_group(array(
                'key' => 'group_5dd69e798c00f',
                'title' => 'Contact',
                'fields' => array(
                    array(
                        'key' => 'field_5dd69e8b9d857',
                        'label' => 'ID',
                        'name' => 'id',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5dd69eb29d858',
                        'label' => 'organizationId',
                        'name' => 'organizationid',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5dd69ebe9d859',
                        'label' => 'version',
                        'name' => 'version',
                        'type' => 'number',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'min' => '',
                        'max' => '',
                        'step' => '',
                    ),
                    array(
                        'key' => 'field_5dd859f7a6dcd',
                        'label' => 'Customer No.',
                        'name' => 'customer_no',
                        'type' => 'number',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'min' => '',
                        'max' => '',
                        'step' => '',
                    ),
                    array(
                        'key' => 'field_5dd85a05a6dce',
                        'label' => 'Vendor No.',
                        'name' => 'vendor_no',
                        'type' => 'number',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'min' => '',
                        'max' => '',
                        'step' => '',
                    ),
                    array(
                        'key' => 'field_5dd85a5da6dcf',
                        'label' => 'VAT Registration No.',
                        'name' => 'vat_registration_no',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5dd85a6fa6dd0',
                        'label' => 'Allow Tax Free Invoices',
                        'name' => 'allow_tax_free_invoices',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'field_5dd85acaa6dd2',
                        'label' => 'Contact Persons',
                        'name' => 'contact_persons',
                        'type' => 'repeater',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'collapsed' => '',
                        'min' => 0,
                        'max' => 0,
                        'layout' => 'table',
                        'button_label' => '',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5dd85adca6dd3',
                                'label' => 'Contact Person Salutation',
                                'name' => 'contact_person_salutation',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_5dd85af8a6dd4',
                                'label' => 'Contact Person First Name',
                                'name' => 'contact_person_first_name',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_5dd85b02a6dd5',
                                'label' => 'Contact Person Last Name',
                                'name' => 'contact_person_last_name',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_5dd85b0aa6dd6',
                                'label' => 'Contact Person Email Address',
                                'name' => 'contact_person_email_address',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                        ),
                    ),
                    array(
                        'key' => 'field_5dd69f349d85c',
                        'label' => 'person',
                        'name' => 'person',
                        'type' => 'textarea',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'maxlength' => '',
                        'rows' => '',
                        'new_lines' => '',
                    ),
                    array(
                        'key' => 'field_5dd69f489d85d',
                        'label' => 'addresses',
                        'name' => 'addresses',
                        'type' => 'textarea',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'maxlength' => '',
                        'rows' => '',
                        'new_lines' => '',
                    ),
                    array(
                        'key' => 'field_5dd69f539d85e',
                        'label' => 'emailAddresses',
                        'name' => 'emailaddresses',
                        'type' => 'textarea',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'maxlength' => '',
                        'rows' => '',
                        'new_lines' => '',
                    ),
                    array(
                        'key' => 'field_5dd69f6d9d85f',
                        'label' => 'phoneNumbers',
                        'name' => 'phonenumbers',
                        'type' => 'textarea',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'maxlength' => '',
                        'rows' => '',
                        'new_lines' => '',
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'contact',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));
        endif;
    }

    /**
     * @param $content
     * @return mixed
     */
    public static function render_account_page($content)
    {
        $user = wp_get_current_user();
        $customer_no = get_field('customer_no', 'user_' . $user->ID);
        $contactController = new ContactController();
        $contact = $contactController->getContact((int)$customer_no);

        include "Views/Account.php";

        return $content;
    }
}

Cairo::boot();