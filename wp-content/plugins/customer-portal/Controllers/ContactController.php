<?php

namespace MabaGroup\Cairo\Controllers;


use MabaGroup\Cairo\Models\Contact;

/**
 * Class ContactController
 * @package MabaGroup\Cairo\Controllers
 */
class ContactController
{
    /**
     * @param int $customerNo
     * @return Contact
     */
    public static function getContact(int $customerNo): Contact
    {
        $args = array(
            "post_type" => "contact",
            "meta_key" => "customer_no",
            "meta_value" => $customerNo,
        );

        $posts = get_posts($args);
        foreach ($posts as $post) {
            return Contact::getContactByWPPost($post);
        }
    }
}