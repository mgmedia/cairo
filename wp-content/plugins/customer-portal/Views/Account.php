<?php
/**
 * @var $contact Contact
 * @var $content string
 */

use MabaGroup\Cairo\Models\Contact;

?>

<div class="my-profile">
    <h3><?php $contact->the_primary_contact_name(); ?></h3>
</div>
