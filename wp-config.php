<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'customerportal' );

/** MySQL database username */
define( 'DB_USER', 'customerportal' );

/** MySQL database password */
define( 'DB_PASSWORD', 'e41y4^Wi' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'yM!7.:Wc+=-;(;y,h|iVlDzTq+Hjw:kk` {j6RTHY3b%v(LK^7)B= ?N.fA:T{+@' );
define( 'SECURE_AUTH_KEY',  'm=m?XbSz>]<qg)pYEvtMbZ4JOZ((8Lp$CZEy^oLw)$cQPjeN?^jlf&W*19BG?QhS' );
define( 'LOGGED_IN_KEY',    'b7].IvkQ_BS2^afT 1FEQK}DSlu*4aw;^Yt]RAOdJY;Hzq&2k|YYB)=/_sI^lW+A' );
define( 'NONCE_KEY',        ' $w8R([-l G>HbR2Kb^q%`9XztVI8}XAR68aVxW;]8y5&*1;0;=* cZ#6|IE<C?Z' );
define( 'AUTH_SALT',        '?a7Q5sQcsMWth]v<69>10(Wvet-%D~#>E[&NVQ0[It1zVV60Jan$k5R)jCdGWjnt' );
define( 'SECURE_AUTH_SALT', 'K0!>I2z3S.q%;)3O1Y*uh_]/Fz{k9$%g01]tynBf@IAQ Kw]-SKE1wHU%xnK9cZQ' );
define( 'LOGGED_IN_SALT',   'zLJ^`-KgNLanFN[1E$(4>|{D(cvqL-Hd?OwS>Mz4A4)Q7E/JB}cB8{|>VLuBU.a5' );
define( 'NONCE_SALT',       'cg66vD+% g;E9/=9}6i:J}il*t1+F<p!=#wzYdyFS_Mo[23yB=l)[sNt*`e,XFpn' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
